# Dokterkucing

[College Project]

Aplikasi sederhana untuk mendeteksi penyakit Kucing

Lihatlah [demo dokterkucing](https://www.youtube.com/watch?v=Xop5ZcIzM1k)

## Quickstart

- `(ql:quickload "dokterkucing")`
- `(dokterkucing:start :port 8000)`
- `(dokterkucing:stop)`

## Credits

Library or Application used by this project

- Hunchentoot Web Server
- Caveman2 Web Framework
- Djula HTML Templating System
- Twitter Bootrstrap
- Awesome Icons
- [Material Design Switch](https://bootsnipp.com/snippets/featured/material-design-switch). [MIT license].
- [Cat Vectors](https://www.freepik.com/free-vector/coloured-cats-collection_1072063.htm) by [Terdpongvector](https://www.freepik.com/terdpongvector).

## Author

* RP Tim X

## Copyright

Copyright (c) 2017 RP Tim X
