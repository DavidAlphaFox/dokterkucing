(in-package :cl-user)
(defpackage dokterkucing.web
  (:use :cl
        :caveman2
        :dokterkucing.config
        :dokterkucing.view
        :dokterkucing.db
        :datafly
        :sxql)
  (:export :*web*))
(in-package :dokterkucing.web)

;; for @route annotation
(syntax:use-syntax :annot)

;;
;; Application

(defclass <web> (<app>) ())
(defvar *web* (make-instance '<web>))
(clear-routing-rules *web*)

;;
;; Routing rules

(defroute "/" ()
  (render #P"index.html"))

(defvar *answers* nil)
(defvar *penanganan* nil)

(defun parse-params (input)
  (remove "name" (alexandria:flatten 
                  (loop for (key . value)
                     in input collect value)) :test #'equal))

(defun get-result (input)
  (cond
    ((equal input '("g1" "g2" "g3" "g4")) (setf *answers* "Distemper"))
    ((equal input '("g1" "g3" "g5")) (setf *answers* "Otitis"))
    ((equal input '("g1" "g3" "g4" "g6")) (setf *answers* "Calivi Virus"))
    ((equal input '("g3" "g7" "g8" "g9")) (setf *answers* "Clamidia"))
    ((equal input '("g3" "g9" "g10")) (setf *answers* "Flu Kucing"))
    ((equal input '("g11" "g12" "g13" "g14" "g15")) (setf *answers* "Scabies"))
    ((equal input '("g12" "g14" "g15" "g16")) (setf *answers* "Ringworm"))
    (t (setf *answers* "Penyakit anda tidak terdeteksi oleh sistem kami, silahkan hubungi dokter terdekat"))))
    

(defun get-penanganan (input)
  (cond
    ((equal input '("g1" "g2" "g3" "g4")) (setf *penaganan* "Bawa Ke dokter"))
    ((equal input '("g1" "g3" "g5")) (setf *penaganan* "foo"))
    ((equal input '("g1" "g3" "g4" "g6")) (setf *penaganan* "foo"))
    ((equal input '("g3" "g7" "g8" "g9")) (setf *penaganan* "foo"))
    ((equal input '("g3" "g9" "g10")) (setf *penaganan* "foo"))
    ((equal input '("g11" "g12" "g13" "g14" "g15")) (setf *penaganan* "foo"))
    ((equal input '("g12" "g14" "g15" "g16")) (setf *penaganan* "foo"))))


    
;; @route GET "/cbdua"
;; (defun cari-jawaban (&key _parsed)
;;   (format nil "~a" _parsed))


@route GET "/inference"
(defun cari-jawaban (&key _parsed)
  (render #P"inferensi.html"
          `(:jawaban ,(get-result (parse-params _parsed))
                     :penanganan ,(get-penanganan (parse-params _parsed)))))


;;
;; Error pages

(defmethod on-exception ((app <web>) (code (eql 404)))
  (declare (ignore app))
  (merge-pathnames #P"_errors/404.html"
                   *template-directory*))
