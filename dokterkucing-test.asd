(in-package :cl-user)
(defpackage dokterkucing-test-asd
  (:use :cl :asdf))
(in-package :dokterkucing-test-asd)

(defsystem dokterkucing-test
  :author "RP Tim X"
  :license ""
  :depends-on (:dokterkucing
               :prove)
  :components ((:module "t"
                :components
                ((:file "dokterkucing"))))
  :perform (load-op :after (op c) (asdf:clear-system c)))
